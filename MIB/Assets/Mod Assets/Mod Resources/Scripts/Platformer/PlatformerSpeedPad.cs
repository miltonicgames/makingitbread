﻿using System.Collections;
using UnityEngine;
using Platformer.Mechanics;

public class PlatformerSpeedPad : MonoBehaviour
{
    public float speed;

    [Range (0, 5)]
    public float duration = 1f;

    void OnTriggerEnter2D(Collider2D other){
        var rb = other.attachedRigidbody;
        if (rb == null) return;
        var player = rb.GetComponent<PlayerController>();
        if (player == null) return;
        player.StartCoroutine(PlayerModifier(player, duration));
    }

    IEnumerator PlayerModifier(PlayerController player, float lifetime){
        var initialSpeed = player.speed;
        player.speed = speed;
        yield return new WaitForSeconds(lifetime);
        player.speed = initialSpeed;
    }

}
