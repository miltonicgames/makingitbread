﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Platformer.Gameplay;
using static Platformer.Core.Simulation;
using Platformer.Model;
using Platformer.Core;

 public class SliceBehavior : MonoBehaviour
    {
        private Queue<Vector3> path = new Queue<Vector3>();
        private float queueTimer;

        private int kongaIndex = 0;
        public float personalSpace = 0.25f;
        public int maxNofPositions = 8;
        private Queue<Vector3> latestPositions;

        // Start is called before the first frame update
        void Start()
        {
            // Initialize the queue with set max number of stored positions
            latestPositions = new Queue<Vector3>(maxNofPositions + 1);

            kongaIndex = GameObject.Find("Player").transform.childCount;

            transform.position += new Vector3(transform.parent.position.x + (personalSpace * kongaIndex), transform.parent.position.y, transform.position.z - 0.01f);
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 lastParentPos = new Vector3(transform.parent.position.x + personalSpace, // Cheat to get some personal space
                                            transform.parent.position.y,
                                            transform.parent.position.z + 0.01f); // Cheat to get the correct Z-buffer 

            // Add the current position to the queue of latest positions
            latestPositions.Enqueue(lastParentPos);
            if (latestPositions.Count > maxNofPositions)
                latestPositions.Dequeue();

            transform.position = latestPositions.Peek();
        }
    }

